function distance(a, b) {
    return Math.sqrt(
        (a.x - b.x) * (a.x - b.x) +
        (a.y - b.y) * (a.y - b.y)
    )
}

function randomPoint() {
    return {
        x: Math.random(),
        y: Math.random(),
    }
}

function randomPoints(amount) {
    const points = [];

    for (let i = 0; i < amount; i++)
        points.push(randomPoint());

    return points;
}

function initialIndexes(points) {
    const indexes = [];

    for (let i = 0; i < points.length; i++)
        indexes.push(i);

    return indexes;
}

function pathLength(points, indexes) {
    const [first, ...rest] = indexes;
    let last = first;
    let sum = 0;

    for (const point of rest) {
        sum += distance(points[last], points[point])
        last = point;
    }

    return sum + distance(points[last], points[first]);
}

function applySwap(indexes, swap) {
    const { a, b } = swap;
    const temp = indexes[a];
    indexes[a] = indexes[b];
    indexes[b] = temp;
}

function generateSwap(points) {
    return {
        a: Math.floor(Math.random() * points.length),
        b: Math.floor(Math.random() * points.length),
    }
}

function evaluateSwap(points, indexes, swap) {
    const { a, b } = swap;

    const oldLengthA = distance(
        points[indexes[(a - 1 + indexes.length) % indexes.length]],
        points[indexes[a]]
    ) + distance(
        points[indexes[a]],
        points[indexes[(a + 1 + indexes.length) % indexes.length]]
    )

    const oldLengthB = distance(
        points[indexes[(b - 1 + indexes.length) % indexes.length]],
        points[indexes[b]]
    ) + distance(
        points[indexes[b]],
        points[indexes[(b + 1 + indexes.length) % indexes.length]]
    )

    // Swap to test
    applySwap(indexes, swap);

    const newLengthA = distance(
        points[indexes[(a - 1 + indexes.length) % indexes.length]],
        points[indexes[a]]
    ) + distance(
        points[indexes[a]],
        points[indexes[(a + 1 + indexes.length) % indexes.length]]
    )

    const newLengthB = distance(
        points[indexes[(b - 1 + indexes.length) % indexes.length]],
        points[indexes[b]]
    ) + distance(
        points[indexes[b]],
        points[indexes[(b + 1 + indexes.length) % indexes.length]]
    )

    // Swap back to original
    applySwap(indexes, swap);

    return newLengthA + newLengthB - oldLengthA - oldLengthB;
}

// Generate new points
// const fs = require("fs/promises");
// const points = randomPoints(10000);
// fs.writeFile("./points.json", JSON.stringify(points));

// use existing points for consitency
const points = require("./points.json");

const indexes = initialIndexes(points);

let score = pathLength(points, indexes);
let swaps = 0;
let effectiveSwaps = 0;
let lastCheckin = new Date();

// console.log(points);
// console.log(indexes);

console.log(`Score: ${score}`);

// console.log(-1 % 5);

setInterval(() => {
    const iterations = 100000;

    for (let i = 0; i < iterations; i++) {
        const swap = generateSwap(points);
        const evaluation = evaluateSwap(points, indexes, swap)

        if (evaluation < 0) {
            applySwap(indexes, swap);
            score += evaluation;
            effectiveSwaps++;
        }
    }

    swaps += iterations;
}, 0)

setInterval(() => {
    const time = new Date();

    console.log();
    console.log(`Time passed:    ${(time - lastCheckin) / 1000.0} s`);
    console.log(`Swaps:          ${swaps}`);
    console.log(`EffectiveSwaps: ${effectiveSwaps}`);
    console.log(`Effectiveness:  ${((effectiveSwaps / swaps) * 100).toFixed(4)} %`);
    console.log(`Swaps / sec:    ${(swaps / (time - lastCheckin) * 1000.0).toFixed(0)} s`);
    console.log(`Score:          ${score}`);

    swaps = 0;
    effectiveSwaps = 0;
    lastCheckin = time;
}, 1000)
